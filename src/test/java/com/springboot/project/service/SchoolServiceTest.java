package com.springboot.project.service;

import com.springboot.project.dao.SchoolDAO;
import com.springboot.project.dto.MarksModelDto;
import com.springboot.project.dto.MarksRequestModelDto;
import com.springboot.project.dto.StudentModelDto;
import com.springboot.project.dto.StudentRequestModelDto;
import com.springboot.project.model.*;
import com.springboot.project.model.mapper.MarksRequestModelMapper;
import com.springboot.project.model.mapper.StudentRequestModelMapper;
import com.springboot.project.validator.Validation;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class SchoolServiceTest {

    @Autowired
    SchoolService schoolService;
    @MockBean
    SchoolDAO schoolDAO;
    @MockBean
    Validation validation;
    @Autowired
    StudentRequestModelMapper studentRequestModelMapper;
    @Autowired
    MarksRequestModelMapper marksRequestModelMapper;

    public StudentModel getStudent(){
        StudentModel student =new StudentModel();
        student.setStudentStandard(1);
        student.setStudentName("abc");
        student.setStudentRollNo("1");
        return student;
    }
    public StudentRequestModel getStudentRequestModel(){
        StudentRequestModelDto studentRequestModelDto=getStudentRequestModelDto();
        StudentRequestModel studentRequestModel=studentRequestModelMapper.mapDtoToModel(studentRequestModelDto);
        return studentRequestModel;
    }

    public StudentRequestModelDto getStudentRequestModelDto(){
        StudentModelDto studentModelDto=new StudentModelDto();
        StudentRequestModelDto studentRequestModelDto=new StudentRequestModelDto();
        studentModelDto.setStudentName("abc");
        studentModelDto.setStudentRollNo("1");
        studentModelDto.setStudentStandard(1);
        studentRequestModelDto.getStudentModelDtoList().add(studentModelDto);
        return studentRequestModelDto;
    }
    @Test
    public void addStudentToSchool() {
        StudentModel student =getStudent();
        when(schoolDAO.addStudentToSchool(student)).thenReturn(true);
        StudentRequestModel studentRequestModel =getStudentRequestModel();
        when(validation.checkDuplicate(studentRequestModel.getStudentModelList())).thenReturn(false);
        boolean result=schoolService.addStudentToSchool(getStudentRequestModelDto());
        assertEquals(true,result);

    }
    public MarksRequestModelDto getMarksRequestModelDto(){
        MarksRequestModelDto marksRequestModelDto=new MarksRequestModelDto();
        MarksModelDto marksModelDto =new MarksModelDto();
        marksModelDto.setSubject("Music");
        marksModelDto.setMarks("50");
        marksModelDto.setRollNo("1");
        marksModelDto.setStandard(1);
        marksRequestModelDto.getMarksModelDtoList().add(marksModelDto);
        return marksRequestModelDto;
    }
    @Test
    void deleteStudent() {
        when(schoolDAO.deleteStudent(getStudent())).thenReturn(true);
        when(validation.exist(Mockito.any())).thenReturn(true);
        boolean result=schoolService.deleteStudent(getStudentRequestModelDto());
        assertEquals(true,result);
    }
    @Test
    void enterOrUpdateMarks() {
        MarksRequestModel marksRequestModel=marksRequestModelMapper.mapDtoToModel(getMarksRequestModelDto());
        when(schoolDAO.enterMarks(marksRequestModel.getMarksModelList().get(0))).thenReturn(true);
        when(validation.exist(Mockito.anyString(),Mockito.anyInt(),Mockito.anyString())).thenReturn(true);
        boolean result=schoolService.enterOrUpdateMarks(getMarksRequestModelDto());
        assertEquals(true,result);
    }
    @Test
    void searchStudents() {
        when(schoolDAO.getStudent("1",1)).thenReturn(getStudent());
        when(validation.exist("1",1)).thenReturn(true);
        List<StudentWithSameRollNo> list=schoolService.searchStudents("1");
        assertEquals("abc",list.get(0).getStudent().getStudentName());
    }
    @Test
    void getMarksByRollNo() {
        when(validation.exist("1",1)).thenReturn(true);
        when(schoolDAO.getStudent("1",1)).thenReturn(getStudent());
        StudentWithSameRollNo result=schoolService.getMarksByRollNo(1,"1");
        assertEquals("abc",result.getStudent().getStudentName());
    }

}