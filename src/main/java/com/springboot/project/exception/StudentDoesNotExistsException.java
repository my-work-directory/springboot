package com.springboot.project.exception;

/**
 * When client is trying to delete  a student but requested student does not found in database
 */
public class StudentDoesNotExistsException extends RuntimeException {
    public StudentDoesNotExistsException(String message){
        super(message);
    }
}
