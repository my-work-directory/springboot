package com.springboot.project.exception;

/**
 * If marks are not available
 */
public class MarksNotAvailableException  extends  RuntimeException{
    public MarksNotAvailableException(String message){
        super(message);
    }
}
