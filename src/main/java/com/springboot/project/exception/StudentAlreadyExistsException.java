package com.springboot.project.exception;

/**
 *When client is trying to add student who is already present in the database
 */
public class StudentAlreadyExistsException extends RuntimeException {
    public StudentAlreadyExistsException(String message){
        super(message);
    }
}
