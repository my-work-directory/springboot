package com.springboot.project.exception;

/**
 * When client is tying to add the subject which is already present
 */
public class SubjectAlreadyExistsException extends  RuntimeException {
    public SubjectAlreadyExistsException(String message){
        super(message);
    }
}
