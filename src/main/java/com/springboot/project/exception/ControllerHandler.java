package com.springboot.project.exception;

import com.springboot.project.model.response.FailureResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Handles all the exceptions which are eventually passed to the controller of the project
 */
@ControllerAdvice
public class ControllerHandler extends  RuntimeException{

    /**
     * Handles the exception when client is trying to delete  a student but requested student does not found in database
     * @param e contains required information like message and stacktrace
     * @return custom response to the client
     */
    @ExceptionHandler(value = StudentDoesNotExistsException.class)
    public ResponseEntity handleStudentDoesNotExistsException(StudentDoesNotExistsException e){
        return new ResponseEntity(new FailureResponse(e.getMessage(),"NOT_FOUND"),HttpStatus.NOT_FOUND);
    }

    /**
     * Handles the exception when client is trying to add student who is already present in the database
     * @param e contains required information like message and stacktrace
     * @return custom response to the client
     */
    @ExceptionHandler(value=StudentAlreadyExistsException.class)
    public ResponseEntity handleStudentAlreadyExistsException(StudentAlreadyExistsException e){
        return new ResponseEntity(new FailureResponse(e.getMessage(),"BAD_REQUEST"),HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles the exception when credentials are wrong or not present in the database
     * @param e contains required information like message and stacktrace
     * @return custom response to the client
     */
    @ExceptionHandler(value=InvalidCredentialsException.class)
    public ResponseEntity handleInvalidCredentialsExceptions(InvalidCredentialsException e){
        return new ResponseEntity(new FailureResponse(e.getMessage(),"NOT_FOUND"),HttpStatus.NOT_FOUND);
    }

    /**
     * Handles the exception when client is tying to add the subject which is already present
     * @param e contains required information like message and stacktrace
     * @return custom response to the client
     */
    @ExceptionHandler(value=SubjectAlreadyExistsException.class)
    public ResponseEntity handleSubjectAlreadyExistsException(SubjectAlreadyExistsException e){
        return new ResponseEntity(new FailureResponse(e.getMessage(),"BAD_REQUEST"),HttpStatus.BAD_REQUEST);
    }

    /**
     * If marks are not available then this method will handle the exception
     * @param e contains required information like message and stacktrace
     * @return custom response to the client
     */
    @ExceptionHandler(value=MarksNotAvailableException.class)
    public ResponseEntity handleMarksNotAvailableException(MarksNotAvailableException e){
        return new ResponseEntity(new FailureResponse(e.getMessage(),"NOT_FOUND"),HttpStatus.NOT_FOUND);
    }

}
