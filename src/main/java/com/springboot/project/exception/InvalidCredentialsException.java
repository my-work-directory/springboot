package com.springboot.project.exception;

/**
 * Exception when credentials are wrong or not present in the database
 */
public class InvalidCredentialsException extends  RuntimeException {
    public  InvalidCredentialsException(String message){
        super(message);
    }
}
