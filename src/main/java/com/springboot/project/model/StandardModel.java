package com.springboot.project.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;


/**
 *This class objects will be used in repository level
 */
@Getter
@Setter
public class StandardModel {
    /**
     *id of the standard
     */
    private int id;
    /**
     *list of all subjects that belongs to this standard
     */
    private ArrayList<SubjectModel> subjectList=new ArrayList<>();
    /**
     *list of all the students that belongs to this standard
     */
    private ArrayList<StudentModel> studentList=new ArrayList<>();
}
