package com.springboot.project.model;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 *This class objects will be used in repository level
 */
@Getter
@Setter
public class MarksRequestModel {
    /**
     *list of marks
     */
    private ArrayList<MarksModel> marksModelList;
}
