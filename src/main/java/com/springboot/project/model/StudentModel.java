package com.springboot.project.model;

import lombok.Getter;
import lombok.Setter;

/**
 *This class objects will be used in repository level
 */
@Getter
@Setter
public class StudentModel {

        /**
         *roll no of the student
         */
        private String studentRollNo;
        /**
         *name of the student
         */
        private String studentName;
        /**
         *standard of the student
         */
        private int studentStandard;

}
