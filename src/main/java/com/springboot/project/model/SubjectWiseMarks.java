package com.springboot.project.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubjectWiseMarks {
    private String subjectName;
    private String subjectMarks;
}
