package com.springboot.project.model;

import com.springboot.project.model.StudentModel;
import com.springboot.project.model.SubjectWiseMarks;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class StudentWithSameRollNo {
    private StudentModel student;
    private List<SubjectWiseMarks> subjectWiseMarksList=new ArrayList<>();
}
