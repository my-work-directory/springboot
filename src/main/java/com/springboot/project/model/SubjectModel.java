package com.springboot.project.model;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 *This class objects will be used in repository level
 */
@Getter
@Setter
public class SubjectModel {
    /**
     *name of the subject
     */
    private String subjectName;
    /**
     *standard to which this subject belongs
     */
    private int subjectStandard;
    /**
     *Marks of all the students registered in this subject
     */
    private Map<String,String> markSheet=new HashMap<>();

}
