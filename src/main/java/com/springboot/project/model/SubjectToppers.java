package com.springboot.project.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SubjectToppers {
    private StudentModel topper;
    private Float marks;
}
