package com.springboot.project.model;

import lombok.Getter;
import lombok.Setter;

/**
 *Marks model to be used to interact with the database/repository level
 */
@Getter
@Setter
public class MarksModel {
    /**
     *roll no of student
     */
    private String rollNo;
    /**
     *standard of the student
     */
    private int standard;
    /**
     *subject of the student
     */
    private String subject;
    /**
     *marks of the student
     */
    private String  marks;
}
