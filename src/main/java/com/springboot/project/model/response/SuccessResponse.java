package com.springboot.project.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuccessResponse {

    private String error;
    private String message;

    public SuccessResponse(String message,String error){
        {
            this.error=error;
            this.message=message;

        }
    }

}
