package com.springboot.project.model.response;

import com.springboot.project.model.StudentWithSameRollNo;

import java.util.List;

public class StudentsPercentageResponse extends StudentsWithSameRollNoResponse {
    public StudentsPercentageResponse(List list){
        super(list);
    }
}
