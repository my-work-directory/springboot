package com.springboot.project.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FailureResponse {

    private String error;
    private String message;

    public FailureResponse(String message,String error){
        {

            this.error=error;
            this.message=message;

        }
    }
}
