package com.springboot.project.model;

import lombok.Getter;
import lombok.Setter;

/**
 *This class objects will be used in repository level
 */
@Getter
@Setter
public class SubjectRequestModel {
    /**
     *Model of the subject
     */
    private SubjectModel subjectModel;

}
