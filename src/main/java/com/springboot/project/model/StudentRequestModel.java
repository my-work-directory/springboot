package com.springboot.project.model;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 *This class objects will be used in repository level
 */
@Getter
@Setter
public class StudentRequestModel {

    /**
     *List of students
     */
    private List<StudentModel> studentModelList =new ArrayList<>();
}
