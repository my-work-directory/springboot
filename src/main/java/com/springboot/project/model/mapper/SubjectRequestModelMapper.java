package com.springboot.project.model.mapper;

import com.springboot.project.controller.request.SubjectRequest;
import com.springboot.project.dto.SubjectModelDto;
import com.springboot.project.dto.SubjectRequestModelDto;
import com.springboot.project.model.SubjectModel;
import com.springboot.project.model.SubjectRequestModel;
import org.springframework.stereotype.Component;

/**
 *To map the Data transfer objects to the Model which will be used to interact with the
 *  Repository level
 */
@Component
public class SubjectRequestModelMapper {

    /**
     *to map DTO of type SubjectRequestModelDtoto model of type SubjectRequestModel
     * @param subjectRequestModelDto will be mapped to SubjectRequestModel
     * @return object of SubjectRequestModel
     */
    public SubjectRequestModel mapDtoToModel(SubjectRequestModelDto subjectRequestModelDto){
        SubjectModelDto subjectModelDto =subjectRequestModelDto.getSubjectModelDto();
        SubjectRequestModel subjectRequestModel=new SubjectRequestModel();
        SubjectModel subjectModel=new SubjectModel();
        subjectModel.setSubjectName(subjectModelDto.getSubjectName());
        subjectModel.setSubjectStandard(subjectModelDto.getSubjectStandard());
        subjectRequestModel.setSubjectModel(subjectModel);
        return subjectRequestModel;
    }
}
