package com.springboot.project.model.mapper;

import com.springboot.project.dto.StudentModelDto;
import com.springboot.project.dto.StudentRequestModelDto;
import com.springboot.project.model.StudentModel;
import com.springboot.project.model.StudentRequestModel;
import org.springframework.stereotype.Component;

/**
 *To map the Data transfer objects to the Model which will be used to interact with the
 *  Repository level
 */
@Component
public class StudentRequestModelMapper {
    /**
     *Maps Data transfer object of type StudentRequestModelDto to the model of type StudentRequestModel
     * @param studentRequestModelDto will be mapped to the StudentRequestModel
     * @return returns object of StudentRequestModel
     */
    public StudentRequestModel mapDtoToModel(StudentRequestModelDto studentRequestModelDto) {
        StudentRequestModel studentRequestModel = new StudentRequestModel();
        for (StudentModelDto studentModelDto : studentRequestModelDto.getStudentModelDtoList()) {
            StudentModel studentModel = new StudentModel();
            studentModel.setStudentName(studentModelDto.getStudentName());
            studentModel.setStudentRollNo(studentModelDto.getStudentRollNo());
            studentModel.setStudentStandard(studentModelDto.getStudentStandard());
            studentRequestModel.getStudentModelList().add(studentModel);
        }
        return studentRequestModel;
    }
}
