package com.springboot.project.model.mapper;

import com.springboot.project.dto.MarksModelDto;
import com.springboot.project.dto.MarksRequestModelDto;
import com.springboot.project.model.MarksModel;
import com.springboot.project.model.MarksRequestModel;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.Mapping;

import java.util.ArrayList;

/**
 *To map the Data transfer objects to the Model which will be used to interact with the
 * Repository level
 */
@Component
public class MarksRequestModelMapper {
    /**
     *Maps data transfer object of type  MarksRequestModelDto to model of type MarksRequestModel
     * @param marksRequestModelDto  will be mapped to the MarksRequestModel
     * @return Objwct of MarksRequestModel
     */
    public MarksRequestModel mapDtoToModel(MarksRequestModelDto marksRequestModelDto){
        ArrayList<MarksModelDto> marksModelDtoList=marksRequestModelDto.getMarksModelDtoList();
        MarksRequestModel marksRequestModel=new MarksRequestModel();
        ArrayList<MarksModel> marksModelList=new ArrayList<>();
        for(MarksModelDto marksModelDto: marksModelDtoList){
            MarksModel marksModel=new MarksModel();
            marksModel.setMarks(marksModelDto.getMarks());
            marksModel.setRollNo(marksModelDto.getRollNo());
            marksModel.setSubject(marksModelDto.getSubject());
            marksModel.setStandard(marksModelDto.getStandard());
            marksModelList.add(marksModel);
        }
        marksRequestModel.setMarksModelList(marksModelList);
        return marksRequestModel;
    }
}
