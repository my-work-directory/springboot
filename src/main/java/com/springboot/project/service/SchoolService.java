package com.springboot.project.service;

import com.springboot.project.dao.SchoolDAO;
import com.springboot.project.dto.MarksRequestModelDto;
import com.springboot.project.dto.StudentRequestModelDto;
import com.springboot.project.dto.SubjectRequestModelDto;
import com.springboot.project.exception.*;
import com.springboot.project.model.*;
import com.springboot.project.model.mapper.MarksRequestModelMapper;
import com.springboot.project.model.mapper.StudentRequestModelMapper;
import com.springboot.project.model.mapper.SubjectRequestModelMapper;
import com.springboot.project.validator.Validation;
import com.springboot.project.model.StudentWithSameRollNo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.*;

/**
 *
 */
@Slf4j
@Service
public class SchoolService {

    /**
     *Bean which will be used to interact with the repository level
     */
    @Autowired
    SchoolDAO schoolDAO;

    /**
     *Bean of Validation which will validates the required tasks
     */
    @Autowired
    Validation validation;

    /**
     *Bean to map objects to the DTO type
     */
    @Autowired
    StudentRequestModelMapper studentRequestModelMapper;

    /**
     *Bean to map objects to Model type
     */
    @Autowired
    SubjectRequestModelMapper subjectRequestModelMapper;

    /**
     *Bean to map objects to Model type
     */
    @Autowired
    MarksRequestModelMapper marksRequestModelMapper;

    /**
     *To add student to the school
     * @param studentRequestModelDto students credentials
     * @return true or exception
     */
    public boolean addStudentToSchool(StudentRequestModelDto studentRequestModelDto) {

        StudentRequestModel studentRequestModel = studentRequestModelMapper.mapDtoToModel(studentRequestModelDto);

            if (!validation.checkDuplicate(studentRequestModel.getStudentModelList())) {
                //TO DO: Add error code
                for (StudentModel studentModel : studentRequestModel.getStudentModelList()) {
                    schoolDAO.addStudentToSchool(studentModel); }
                }
            else{
                throw new StudentAlreadyExistsException("Student already exists with given credentials");
            }
            return true;
    }
    /**
     *To delete students from the school
     * @param studentRequestModelDto students credentials
     * @return true or exception
     */
    public boolean deleteStudent(StudentRequestModelDto studentRequestModelDto) {
        StudentRequestModel studentRequestModel = studentRequestModelMapper.mapDtoToModel(studentRequestModelDto);

        if (validation.exist(studentRequestModel.getStudentModelList())) {
            for(StudentModel studentModel:studentRequestModel.getStudentModelList()) {
                schoolDAO.deleteStudent(studentModel);
                }
            }
            else {
                throw new StudentDoesNotExistsException("Student not found ");
            }

        return true;
    }
    /**
     *Enter or update the marks
     * @param marksRequestModelDto information about marks
     * @return true or exception
     */
    public boolean enterOrUpdateMarks(MarksRequestModelDto marksRequestModelDto){

        MarksRequestModel marksRequestModel=marksRequestModelMapper.mapDtoToModel(marksRequestModelDto);

        for(MarksModel marks:marksRequestModel.getMarksModelList()){
            if(validation.exist(marks.getRollNo(),marks.getStandard(),marks.getSubject())){
                continue;
            }
            else{
                log.error("Either Student does not exists with given roll no {} and standard {} " +
                        "or Subject does not exist with name {} ", marks.getRollNo(), marks.getStandard(), marks.getSubject());
                throw new InvalidCredentialsException("Student not found with  Roll No: " + marks.getRollNo()+ " Standard: "+ marks.getStandard()+ " Subject: " +marks.getSubject());
            }
        }

        for(MarksModel marks:marksRequestModel.getMarksModelList()){
                schoolDAO.enterMarks(marks);
        }

        return true;
    }

    /**
     * To search student by the roll no
     * @param id roll no of the student
     * @return list of students with same roll no
     */
    public List searchStudents(String id){
        List<StudentWithSameRollNo> list=new ArrayList();
        for(int standard=1;standard<11;standard++){
            if(validation.exist(id,standard)){
                List<SubjectWiseMarks> subjectWiseMarksList =new ArrayList<>();
                Map<String,String> map=schoolDAO.getSubjectAndMarks(id,standard);
                for(String key:map.keySet()){
                    SubjectWiseMarks subjectWiseMarks=new SubjectWiseMarks();
                    subjectWiseMarks.setSubjectName(key);
                    subjectWiseMarks.setSubjectMarks(map.get(key));
                    subjectWiseMarksList.add(subjectWiseMarks);
                }
                StudentModel studentModel=schoolDAO.getStudent(id,standard);
                StudentWithSameRollNo studentWithSameRollNo=new StudentWithSameRollNo();
                studentWithSameRollNo.setStudent(studentModel);
                studentWithSameRollNo.setSubjectWiseMarksList(subjectWiseMarksList);
                list.add(studentWithSameRollNo);
                log.info("Search successful for student with rollNo "+id +"in standard "+standard);
            }
        }
        return list;
    }

    /**
     * To get the Marks of students with given roll no and standard
     * @param standard standard of the student
     * @param rollNo  roll no of the student
     * @return true or exception
     */
    public StudentWithSameRollNo getMarksByRollNo(int standard,String rollNo){
        if(validation.exist(rollNo,standard)){
            List<SubjectWiseMarks> subjectWiseMarksList =new ArrayList<>();
            Map<String,String> map=schoolDAO.getSubjectAndMarks(rollNo,standard);
            for(String key:map.keySet()){
                SubjectWiseMarks subjectWiseMarks=new SubjectWiseMarks();
                subjectWiseMarks.setSubjectName(key);
                subjectWiseMarks.setSubjectMarks(map.get(key));
                subjectWiseMarksList.add(subjectWiseMarks);
            }
            StudentModel studentModel=schoolDAO.getStudent(rollNo,standard);
            StudentWithSameRollNo studentWithSameRollNo=new StudentWithSameRollNo();
            studentWithSameRollNo.setStudent(studentModel);
            studentWithSameRollNo.setSubjectWiseMarksList(subjectWiseMarksList);
            log.info("Marks fetched successfully for rollNo "+rollNo +" in standard "+standard);
            return studentWithSameRollNo;

        }
        else{
            throw new StudentDoesNotExistsException("Student not found with given credentials");
        }
    }

    /**
     *To get all students who are above given percentage  in given standard
     * @param standard standard of the student
     * @param percent percentage value
     * @return list of students
     */
    public List<StudentsWithPercentage> fetchStudentsByPercentage(int standard,float percent){

        List<StudentsWithPercentage> studentsWithPercentageList=new ArrayList<>();
        List<StudentModel> studentModelList=schoolDAO.fetchStudentsByPercentage(standard,percent);

        for(StudentModel studentModel:studentModelList) {
            StudentsWithPercentage studentsWithPercentage = new StudentsWithPercentage();
            studentsWithPercentage.setStudent(studentModel);
            Map<String, String> map = schoolDAO.getSubjectAndMarks(studentModel.getStudentRollNo(), standard);
            List<SubjectWiseMarks> list = new ArrayList<>();
            for (String key : map.keySet()) {
                SubjectWiseMarks subjectWiseMarks = new SubjectWiseMarks();
                subjectWiseMarks.setSubjectName(key);
                subjectWiseMarks.setSubjectMarks(map.get(key));
                list.add(subjectWiseMarks);
            }
            studentsWithPercentage.setSubjectWiseMarksList(list);
            studentsWithPercentageList.add(studentsWithPercentage);
            log.info("fetched students on the basis of percentage {} ",percent);
        }
        return studentsWithPercentageList;
    }

    /**
     *To fetch topper of the given subject in given standard
     * @param standard standard of the student
     * @param subject subject name
     * @return list
     */
    public List fetchToppersBySubject(int standard,String subject){
            List list=schoolDAO.fetchMarksBySubject(standard,subject);
            try{
                Collections.sort(list,Collections.reverseOrder());
                float maximumMarks=(float)list.get(0);
                List<SubjectToppers> toppersList =new ArrayList<>();
                for(StudentModel studentModel:schoolDAO.fetchStudentsByMarks(standard,maximumMarks,subject)){
                    SubjectToppers subjectToppers= new SubjectToppers();
                    subjectToppers.setTopper(studentModel);
                    subjectToppers.setMarks(maximumMarks);
                    toppersList.add(subjectToppers);
                }
                log.info("Fetched toppers successfully ");
                return toppersList;

            }
            catch(Exception e){
                log.info("No students available with marks in given subject "+subject);
                throw new MarksNotAvailableException("Marks not available for this subject");

            }
    }


    /**
     *
     * @param studentRequestModelDto
     * @return
     */
    public boolean editStudent(StudentRequestModelDto studentRequestModelDto) {
        StudentRequestModel studentRequestModel = studentRequestModelMapper.mapDtoToModel(studentRequestModelDto);
        if (validation.exist(studentRequestModel.getStudentModelList())) {
            for(StudentModel studentModel:studentRequestModel.getStudentModelList()) {
            }
        }
        else {
            throw new StudentDoesNotExistsException("Student not found ");
        }

        return true;
    }

    /**
     *To add new subject to the standard
     * @param subjectRequestModeldto subject information
     * @return true or exception
     */
    public boolean addSubjectToStandard(SubjectRequestModelDto subjectRequestModeldto){
        subjectRequestModelMapper=new SubjectRequestModelMapper();
        SubjectRequestModel subjectRequestModel=subjectRequestModelMapper.mapDtoToModel(subjectRequestModeldto);
        if(validation.checkDuplicate(subjectRequestModel.getSubjectModel())){
            log.error("Subject already exists with name {} and standard {}", subjectRequestModel.getSubjectModel().getSubjectName(), subjectRequestModel.getSubjectModel().getSubjectStandard());
            throw new SubjectAlreadyExistsException("Subject already exists");

        }
        return schoolDAO.addSubjectToStandard(subjectRequestModel.getSubjectModel());
    }

}
