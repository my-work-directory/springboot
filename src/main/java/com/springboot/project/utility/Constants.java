package com.springboot.project.utility;


/**
 *This is the class which have final and stactic variables which will be use to
 * initialize the school service
 */
public class Constants {
    private Constants() {
        super();
    }

    public static final int FIRST_STANDARD=1;
    public static final int SECOND_STANDARD=2;
    public static final int THIRD_STANDARD=3;
    public static final int FOURTH_STANDARD=4;
    public static final int FIFTH_STANDARD=5;
    public static final int SIXTH_STANDARD=6;
    public static final int SEVENTH_STANDARD=7;
    public static final int EIGHTH_STANDARD=8;
    public static final int NINTH_STANDARD=9;
    public static final int TENTH_STANDARD=10;

    public static final String MUSIC="Music";
    public static final String ARTS="Arts";
    public static final String SPORTS="Sports";
    public static final String SUPW="Socially Useful Productive Work";
    public static final String SOCIAL_SCIENCE="Social Science";


}
