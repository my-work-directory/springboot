package com.springboot.project.validator;

import com.springboot.project.dao.SchoolDAO;
import com.springboot.project.model.StandardModel;
import com.springboot.project.model.StudentModel;
import com.springboot.project.model.SubjectModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *Validates the required tasks
 */
@Slf4j
@Component
public class Validation {

    @Autowired
    SchoolDAO schoolDAO;

    /**
     *Check if a student already present in the database/storage
     * @param studentModelList list of students to be checked
     * @return true even if a single student in the list already exists in the storage
     */
    public boolean checkDuplicate(List<StudentModel> studentModelList){

        for (StudentModel studentModel : studentModelList) {
            boolean foundStudent=false;
            StandardModel standardModel = schoolDAO.getStandardList().get(studentModel.getStudentStandard());
            for (StudentModel student : standardModel.getStudentList()) {
                if (student.getStudentRollNo().equals(studentModel.getStudentRollNo())) {
                    foundStudent=true;
                }
            }
            if(foundStudent){
                log.error("Student already exists with given standard {} and roll no {}",
                        studentModel.getStudentStandard(), studentModel.getStudentRollNo());
                return true;
            }
        }
        return false;
    }


    /**
     *Checks if a subject is present already
     * @param subjectModel Subject information
     * @return true or false
     */
    public boolean checkDuplicate(SubjectModel subjectModel){
        StandardModel standardModel=schoolDAO.getStandardList().get(subjectModel.getSubjectStandard());
        for(SubjectModel subject:standardModel.getSubjectList()){
            if(subjectModel.getSubjectName().equals(subject.getSubjectName())){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if student already exists with given roll no and standard
     * @param rollNo roll no of the student
     * @param standard standard of the student
     * @return true if students is present
     */
    public boolean exist(String rollNo,int standard){
        StandardModel standardModel= schoolDAO.getStandardList().get(standard);
        boolean rollNoFound=false;
        for(StudentModel student:standardModel.getStudentList()){
            if(rollNo.equals(student.getStudentRollNo())){
                rollNoFound=true;
                break;
            }
        }
        return rollNoFound;
    }

    /**
     *Checks if a student is present in the database with given roll no,standard,subject
     * @param rollNo roll no of the student
     * @param standard standard of the student
     * @param subject subject of the student
     * @return true or false
     */
    public boolean exist(String rollNo,int standard,String subject) {
        StandardModel standardModel= schoolDAO.getStandardList().get(standard);
        boolean subjectFound=false;
        if(!exist(rollNo,standard)){
            log.error("Roll no not found in database for standard {} ,roll no {}",standard,rollNo);
            return false;
        }
        for(SubjectModel subjectModel:standardModel.getSubjectList()){
            if(subject.equals(subjectModel.getSubjectName())){
                subjectFound=true;
                break;
            }
        }
        if(!subjectFound){
            return false;
        }
        return true;
    }

    /**
     *To check if students are present in the database
     * @param studentModelList list of the students
     * @return false if atleast one is not present
     */
    public boolean exist(List<StudentModel> studentModelList) {

        for (StudentModel studentModel : studentModelList) {
            boolean foundStudent=false;
            StandardModel standardModel = schoolDAO.getStandardList().get(studentModel.getStudentStandard());
            for (StudentModel student : standardModel.getStudentList()) {
                if (student.getStudentRollNo().equals(studentModel.getStudentRollNo())) {
                    foundStudent=true;
                }
            }
            if(!foundStudent){
                log.error("Student does not exists with given roll no {} and standard {}",
                        studentModel.getStudentRollNo(), studentModel.getStudentStandard());
                return false;
            }
        }
        return true;
    }
}
