package com.springboot.project.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *This is the Data transfer object corresponding to the type SubjectRequest which will be used by service for the further processing
 */
@Getter
@Setter
public class SubjectRequestModelDto {
    /**
     *Data transfer object of the type Subject
     */
    private SubjectModelDto subjectModelDto;
}
