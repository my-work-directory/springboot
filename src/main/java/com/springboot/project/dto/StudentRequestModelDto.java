package com.springboot.project.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 *This is the Data transfer object corresponding to the type StudentRequest which will be used by service for the further processing
 */
@Getter
@Setter
public class StudentRequestModelDto {
    /**
     *List of the students
     */
    private List<StudentModelDto> studentModelDtoList=new ArrayList<>();
}
