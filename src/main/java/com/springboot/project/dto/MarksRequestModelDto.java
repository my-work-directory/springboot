package com.springboot.project.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 *This is the Data transfer object which will be used by service for the further processing
 */
@Getter
@Setter
public class MarksRequestModelDto {
    /**
     *List of marks(represented by type MarksModelDto)
     */
    private ArrayList<MarksModelDto> marksModelDtoList=new ArrayList<>();

}
