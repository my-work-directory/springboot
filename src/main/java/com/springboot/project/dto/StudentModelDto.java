package com.springboot.project.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *This is the Data transfer object corresponding to the StudentModel type (in the controller.request package )
 * which will be used by service for the further processing
 */
@Getter
@Setter
public class StudentModelDto {
    /**
     *Roll no of the student
     */
    private String studentRollNo;
    /**
     *Name of the student
     */
    private String studentName;
    /**
     * standard of the student
     */
    private int studentStandard;
}
