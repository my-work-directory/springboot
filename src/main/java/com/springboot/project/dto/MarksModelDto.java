package com.springboot.project.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *This is the Data transfer object which will be used by service for the further processing
 */
@Getter
@Setter
public class MarksModelDto {
    /**
     *Roll no of the student
     */
    private String rollNo;
    /**
     *Standard of the student
     */
    private int standard;
    /**
     *Subject of the student
     */
    private String subject;
    /**
     *Marks got by the student
     */
    private String  marks;
}
