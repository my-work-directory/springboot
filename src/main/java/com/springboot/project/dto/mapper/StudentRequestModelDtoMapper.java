package com.springboot.project.dto.mapper;

import com.springboot.project.controller.request.Student;
import com.springboot.project.controller.request.StudentRequest;
import com.springboot.project.dto.StudentModelDto;
import com.springboot.project.dto.StudentRequestModelDto;
import org.springframework.stereotype.Component;

/**
 *To map java classes (obtained after request from the client) to the
 * new Data Transfer Objects which will be used by service for further processing
 */
@Component
public class StudentRequestModelDtoMapper {
    /**
     *Maps Request object of type StudentRequest objects to the Data transfer objects
     * of type StudentRequestModelDto
     * @param studentRequest instance of StudentRequest which will be mapped to an instance of
     *      *                      StudentRequestModelDto
     * @return object of the StudentRequestModelDto
     */
    public StudentRequestModelDto mapRequestToDto(StudentRequest studentRequest){
        StudentRequestModelDto studentRequestModelDto =new StudentRequestModelDto();
        for(Student student: studentRequest.getStudent()) {
            StudentModelDto studentModelDto = new StudentModelDto();
            studentModelDto.setStudentName(student.getStudentName());
            studentModelDto.setStudentRollNo(student.getStudentRollNo());
            studentModelDto.setStudentStandard(student.getStudentStandard());
            studentRequestModelDto.getStudentModelDtoList().add(studentModelDto);
        }
        return studentRequestModelDto;
    }
}
