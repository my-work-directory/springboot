package com.springboot.project.dto.mapper;

import com.springboot.project.controller.request.SubjectRequest;
import com.springboot.project.dto.SubjectModelDto;
import com.springboot.project.dto.SubjectRequestModelDto;
import org.springframework.stereotype.Component;

/**
 *To map java classes (obtained after request from the client) to the
 * new Data Transfer Objects which will be used by service for further processing
 */
@Component
public class SubjectRequestModelDtoMapper {
    /**
     *Maps Request object of type SubjectRequest objects to the Data transfer objects
     *  of type SubjectRequestModelDto
     * @param subjectRequest instance of SubjectRequest which will be mapped to an instance of
     *                       SubjectRequestModelDto
     * @return object of the SubjectRequestModelDto
     */
    public SubjectRequestModelDto mapRequestToDto(SubjectRequest subjectRequest){
        SubjectRequestModelDto subjectRequestModeldto=new SubjectRequestModelDto();
        SubjectModelDto subjectModelDto=new SubjectModelDto();
        subjectModelDto.setSubjectName(subjectRequest.getSubject().getSubjectName());
        subjectModelDto.setSubjectStandard(subjectRequest.getSubject().getSubjectStandard());
        subjectRequestModeldto.setSubjectModelDto(subjectModelDto);
        return subjectRequestModeldto;
    }
}
