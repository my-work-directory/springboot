package com.springboot.project.dto.mapper;

import com.springboot.project.controller.request.Marks;
import com.springboot.project.controller.request.MarksRequest;
import com.springboot.project.dto.MarksModelDto;
import com.springboot.project.dto.MarksRequestModelDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 *To map java classes (obtained after request from the client) to the
 * new Data Transfer Objects which will be used by service for further processing
 */
@Component
public class MarksRequestModelDtoMapper {
    /**
     *Maps MarksRequest to a new Data Transfer Object - MarksRequestModelDto
     * @param marksRequest  instance of MarksRequest which will be mapped to an instance of
     *                      MarksRequestModelDto
     * @return instance of MarksRequestModelDto
     */
    public MarksRequestModelDto mapReuqestToDto(MarksRequest marksRequest){
        ArrayList<Marks> marksList= marksRequest.getMarksList();
        MarksRequestModelDto marksRequestModelDto =new MarksRequestModelDto();
        ArrayList<MarksModelDto> marksModelDtoList=new ArrayList<>();
        for(Marks marksElement:marksList){
            MarksModelDto marksModelDto = new MarksModelDto();
            marksModelDto.setMarks(marksElement.getMarks());
            marksModelDto.setRollNo(marksElement.getRollNo());
            marksModelDto.setStandard(marksElement.getStandard());
            marksModelDto.setSubject(marksElement.getSubject());
            marksModelDtoList.add(marksModelDto);
        }
        marksRequestModelDto.setMarksModelDtoList(marksModelDtoList);
        return marksRequestModelDto;
    }
}
