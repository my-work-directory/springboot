package com.springboot.project.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *This is the Data transfer object corresponding to the type Subject which will be used by service for the further processing
 */
@Getter
@Setter
public class SubjectModelDto {
    /**
     *name of the subject
     */
    private String subjectName;
    /**
     *standard in which this subject should be kept
     */
    private int subjectStandard;
}
