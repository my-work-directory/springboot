package com.springboot.project.config;

import com.springboot.project.model.StandardModel;
import com.springboot.project.model.SubjectModel;
import com.springboot.project.utility.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;


/**
 *This class is for creating the bean of in memory storage
 * which acts as database for this project
 */
@Component
public class StandardList {

    /**
     * This is resizable array which is actual storage
     * unit for the entire project since it is resizable
     * it helps in adding new elements easily and also helps
     * in deleting required elements
     */
    ArrayList<StandardModel> standardList=new ArrayList<>();

    /**
     * This method is to initialize default elements (subjects/standards)
     * in the storage
     * @return the bean for the storage with default subjects and standards
     */
    @Bean
    public ArrayList<StandardModel> createStandardList() {

        standardList.add(new StandardModel());
        for (int i = 1; i < 11; i++) {
            StandardModel standardModel = new StandardModel();
            addStandard(i, standardModel);
            addSubjects(i,standardModel);
            standardList.add(standardModel);
        }
        return standardList;
    }

    /**
     *To add all default subjects to a particular standard (@param standardId)
     * @param standardId a particular standard
     * @param standardModel model for a standard in school
     */
    private void addSubjects(int standardId,StandardModel standardModel){
        standardModel.getSubjectList().add(addSubjects(standardId,Constants.MUSIC));
        standardModel.getSubjectList().add(addSubjects(standardId,Constants.ARTS));
        standardModel.getSubjectList().add(addSubjects(standardId,Constants.SPORTS));
        standardModel.getSubjectList().add(addSubjects(standardId,Constants.SUPW));
        standardModel.getSubjectList().add(addSubjects(standardId,Constants.SOCIAL_SCIENCE));

    }

    /**
     *To add a single subject to a particular standard (@param standardId)
     * @param standardId a particular standard
     * @param subject name of a subject to be added to a particular standard
     * @return an instance of <tt>SubjectModel</tt>
     */
    private SubjectModel addSubjects(int standardId,String subject){
        SubjectModel subjectModel=new SubjectModel();
        subjectModel.setSubjectName(subject);
        subjectModel.setSubjectStandard(standardId);
        return subjectModel;
    }

    /**
     *To add a standard to the school
     * @param standardId standard id
     * @param standardModel model for a standard in school
     */
    private void addStandard(int standardId, StandardModel standardModel) {
        switch (standardId) {
            case (1):
                standardModel.setId(Constants.FIRST_STANDARD);
                break;
            case (2):
                standardModel.setId(Constants.SECOND_STANDARD);
                break;
            case (3):
                standardModel.setId(Constants.THIRD_STANDARD);
                break;
            case (4):
                standardModel.setId(Constants.FOURTH_STANDARD);
                break;
            case (5):
                standardModel.setId(Constants.FIFTH_STANDARD);
                break;
            case (6):
                standardModel.setId(Constants.SIXTH_STANDARD);
                break;
            case (7):
                standardModel.setId(Constants.SEVENTH_STANDARD);
                break;
            case (8):
                standardModel.setId(Constants.EIGHTH_STANDARD);
                break;
            case (9):
                standardModel.setId(Constants.NINTH_STANDARD);
                break;
            case (10):
                standardModel.setId(Constants.TENTH_STANDARD);
                break;
        }
    }
}
