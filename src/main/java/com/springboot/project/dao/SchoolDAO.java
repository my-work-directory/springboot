package com.springboot.project.dao;


import com.springboot.project.model.*;


import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *This class is to interact with the database(i.e in memory storage)
 */
@Slf4j
@Getter
@Setter
@Repository
public class SchoolDAO {

    /**
     *This is the database/storage for this project
     */
    @Autowired
    ArrayList<StandardModel> standardList;

    /**
     *To add a new student to the database
     * @param student credentials of the student
     * @return true
     */
    public boolean addStudentToSchool(StudentModel student){

        int standard=student.getStudentStandard();
        String rollNo=student.getStudentRollNo();
        String name=student.getStudentName();
        StudentModel studentModel =new StudentModel();
        studentModel.setStudentRollNo(rollNo);
        studentModel.setStudentName(name);
        studentModel.setStudentStandard(standard);

        standardList.get(standard).getStudentList().add(studentModel);
        log.info("Added student to school with standard {} and roll no {}",standard,rollNo);
        return true;
    }

    /**
     *To delete the student from the database/storage
     * @param student redentials of the student
     * @return true
     */
    public boolean deleteStudent(StudentModel student){
        StandardModel standardModel=standardList.get(student.getStudentStandard());

        for(StudentModel studentModel:standardModel.getStudentList()){
            if(studentModel.getStudentRollNo().equals(student.getStudentRollNo())){
                standardModel.getStudentList().remove(studentModel);
                break;
            }
        }
        for(SubjectModel subjectModel:standardModel.getSubjectList()){
            subjectModel.getMarkSheet().remove(student.getStudentRollNo());
        }
        log.info("Student deleted from school with standard {} and roll no {}",
                student.getStudentStandard(),student.getStudentRollNo());
        return true;
    }

    /**
     *Enter the marks in the database
     * @param marksModel marks with required information to
     * @return true
     */
    public boolean enterMarks(MarksModel marksModel){

            String rollNo=marksModel.getRollNo();
            int standard=marksModel.getStandard();
            String subject=marksModel.getSubject();
            String marks=marksModel.getMarks();

            for(SubjectModel subjectModel:standardList.get(standard).getSubjectList()){
                if(subjectModel.getSubjectName().equals(subject)){
                    subjectModel.getMarkSheet().put(rollNo,marks);
                    log.info("Entered marks for student with standard {} ,roll no {} and subject {}", standard,rollNo,subject);
                }

            }

        return true;
    }

    /**
     * To get subject wise marks of the student
     * @param rollNo roll no of the student
     * @param standard standard of the student;
     * @return map containing key as subjects and value as marks
     */
    public Map<String,String> getSubjectAndMarks(String rollNo, int standard){
        Map<String,String> subjectMarks=new HashMap<>();
        for(SubjectModel subjectModel:standardList.get(standard).getSubjectList()){
            subjectMarks.put(subjectModel.getSubjectName(),subjectModel.getMarkSheet().get(rollNo));
        }
        return subjectMarks;
    }

    /**
     * To get credentials of a student
     * @param rollNo roll no of the student
     * @param standard standard of the student;
     * @return model for the student
     */
    public StudentModel getStudent(String rollNo,int standard){
        for(StudentModel studentModel:standardList.get(standard).getStudentList()){
            if(studentModel.getStudentRollNo().equals(rollNo)){
                return studentModel;
            }
        }
        return null;
    }

    /**
     * To get the all students who scored more than the required percentage
     * @param standard standard to look for
     * @param percent minimum percentage value
     * @return list of students who scores more than <>percent</>
     */
    public List<StudentModel> fetchStudentsByPercentage(int standard,float percent) {
        List<StudentModel> studentModelList=new ArrayList<>();
        for (StudentModel studentModel : standardList.get(standard).getStudentList()) {
            if(calculatePercentage(standard,studentModel.getStudentRollNo())>percent){
                studentModelList.add(studentModel);
            }
        }
        return studentModelList;
    }

    /**
     * To calculate the percentage of the student with <>standard</> and <>rollNo</>
     * @param standard standard of the student
     * @param rollNo roll of of the student
     * @return percentage value
     */
    public float calculatePercentage(int standard,String rollNo){
        float totalMarks=0;
        try {
            for (SubjectModel subjectModel : standardList.get(standard).getSubjectList()) {
                subjectModel.getMarkSheet().get(rollNo);
                totalMarks = totalMarks + Float.valueOf(subjectModel.getMarkSheet().get(rollNo));
            }
        }
        catch (Exception e){
            log.info("Marks not available for Roll No: "+rollNo+" in standard "+standard);
        }
        return totalMarks/standardList.get(standard).getSubjectList().size();
    }

    /**
     * To get marks list of the students for a particular subject and standard
     * @param standard standard id of the standard
     * @param subject name of the subject
     * @return list of marks for the particular subject for each student
     */
    public List fetchMarksBySubject(int standard,String subject){
        List list = new ArrayList<>();
        for (SubjectModel subjectModel:standardList.get(standard).getSubjectList()){
            if(subjectModel.getSubjectName().equals(subject)) {

                for (String key : subjectModel.getMarkSheet().keySet()) {
                    try {
                        list.add(Float.valueOf(subjectModel.getMarkSheet().get(key)));
                    }
                    catch(Exception e){
                        log.info("Marks not available for "+key+" in standard "+standard);
                    }

                }
            }
        }
        return list;
    }

    /**
     * To get the list of students who score a particular mark<>marks</> in particular subject<>subject</>
     * @param standard students from only standard:<>standard</> should be  included in the list
     * @param marks marks to search for
     * @param subject subject name to search for
     * @return list of students who scored particular mark<>marks</> in particular subject<>subject</>
     */
    public List<StudentModel> fetchStudentsByMarks(int standard,float marks,String subject) {
        List<StudentModel> studentModelList =new ArrayList<>();
        for (SubjectModel subjectModel : standardList.get(standard).getSubjectList()) {
            if (subjectModel.getSubjectName().equals(subject)) {
                for (String key : subjectModel.getMarkSheet().keySet()) {
                    try {
                        if (Float.valueOf(subjectModel.getMarkSheet().get(key)) == marks){
                            studentModelList.add(getStudent(key,standard));
                        }

                    }
                    catch(Exception e){
                        log.info("Marks not available for "+key);
                    }
                }
            }
        }
        return studentModelList;
    }

    /**
     *To add the new subject in the database
     * @param subject name of the subject
     * @return true
     */
    public boolean addSubjectToStandard(SubjectModel subject){
        String name=subject.getSubjectName();
        int standard=subject.getSubjectStandard();

        SubjectModel subjectModel =new SubjectModel();
        subjectModel.setSubjectName(name);
        subjectModel.setSubjectStandard(standard);
        standardList.get(standard).getSubjectList().add(subjectModel);
        log.info("Added subject:: {} to Standard {}",name,standard);
        return true;
    }
}
