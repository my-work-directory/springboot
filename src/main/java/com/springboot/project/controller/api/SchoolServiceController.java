package com.springboot.project.controller.api;

import com.springboot.project.controller.request.MarksRequest;
import com.springboot.project.controller.request.StudentRequest;
import com.springboot.project.controller.request.SubjectRequest;
import com.springboot.project.model.StudentWithSameRollNo;
import com.springboot.project.model.response.StudentsPercentageResponse;
import com.springboot.project.model.response.StudentsWithSameRollNoResponse;
import com.springboot.project.model.response.SuccessResponse;
import com.springboot.project.dto.mapper.MarksRequestModelDtoMapper;
import com.springboot.project.dto.mapper.StudentRequestModelDtoMapper;
import com.springboot.project.dto.mapper.SubjectRequestModelDtoMapper;
import com.springboot.project.service.SchoolService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *This class is to handle various requests requested by client
 */
@Slf4j
@RestController
@RequestMapping("/school-service")
public class SchoolServiceController {
    /**
     *Bean of SchoolService to use in methods to handover the flow of execution to the
     * service layer
     */
    @Autowired
    SchoolService service;
    /**
     *Bean of Mapper to map a particular input requests to the Data Transfer Objects
     */
    @Autowired
    StudentRequestModelDtoMapper studentRequestModelDtoMapper;
    /**
     *Bean of Mapper to map a particular input requests to the Data Transfer Objects
     */
    @Autowired
    MarksRequestModelDtoMapper marksRequestModelDtoMapper;
    /**
     *Bean of Mapper to map a particular input requests to the Data Transfer Objects
     */
    @Autowired
    SubjectRequestModelDtoMapper subjectRequestModelDtoMapper;

    /**
     *This method handles the request came from client to add a student to the school
     * @param request model for the information of a student got from client
     * @return a response to the client
     */
    @PostMapping(name="createStudentEntry",value="/add-student",consumes = "application/json")
    public ResponseEntity createStudentEntry(@RequestBody StudentRequest request ){
        if(service.addStudentToSchool(studentRequestModelDtoMapper.mapRequestToDto(request))) {
            return new ResponseEntity(new SuccessResponse("Request Successful",""),HttpStatus.OK);
        }
        return null;
    }
    /**
     *This method handles the request came from client to delete the particular students from the database
     * @param request students information which should be deleted from the database
     * @return if successfully deleted then returns success response
     */
    @DeleteMapping(name="deleteStudent",value="/delete-student",consumes = "application/json")
    public ResponseEntity deleteStudent(@RequestBody StudentRequest request){
        if(service.deleteStudent(studentRequestModelDtoMapper.mapRequestToDto(request))){
            return new ResponseEntity(new SuccessResponse("Request Successful",""),HttpStatus.OK);
        }
        return null;
    }
    /**
     *This method handles the request came from client to enter the marks of students
     * @param marksRequest students information along with marks to be entered
     * @return if successfully entered the marks in database then returns success response
     */
    @PostMapping(name="enterMarks",value="/enter-marks",consumes = "application/json")
    public ResponseEntity enterMarks(@RequestBody MarksRequest marksRequest){
        if(service.enterOrUpdateMarks(marksRequestModelDtoMapper.mapReuqestToDto(marksRequest))){
            log.info("Marks entered successfully");
            return new ResponseEntity(new SuccessResponse("Request Successful",""),HttpStatus.OK);
        }
        return null;
    }
    /**
     *This method handles the request came from client to edit the marks of students
     * @param marksRequest students information along with marks to be edited
     * @return if successfully edited the marks in database then returns success response
     */
    @PutMapping(name="enterMarks",value="/edit-marks",consumes = "application/json")
    public ResponseEntity editMarks(@RequestBody MarksRequest marksRequest){
        if(service.enterOrUpdateMarks(marksRequestModelDtoMapper.mapReuqestToDto(marksRequest))){
            log.info("Marks edited successfully");
            return new ResponseEntity(new SuccessResponse("Request Successful",""),HttpStatus.OK);
        }
        return null;
    }

    /**
     * This method handles the request came from client to search students by Roll no
     * @param id Roll no of the student
     * @return the all students from different standards which matches given roll no
     */
    @GetMapping(name="searchStudents",value="/search/students/{id}")
    public ResponseEntity searchStudents(@PathVariable String id){
        List list=service.searchStudents(id);
        return new ResponseEntity(new StudentsWithSameRollNoResponse(list),HttpStatus.OK);
    }

    /**
     * This method handles the request came from client to get subject wise marks of a
     * particular requested student
     * @param standard standard of the particular requested  student
     * @param rollNo roll no of a requested student
     * @return
     */
    @GetMapping(name="getMarksByRollNo",value="/marks/{standard}/{rollNo}")
    public ResponseEntity getMarksByRollNo(@PathVariable int standard, @PathVariable  String rollNo){
        StudentWithSameRollNo studentWithSameRollNo =service.getMarksByRollNo(standard,rollNo);
        return new ResponseEntity(studentWithSameRollNo,HttpStatus.OK);
    }

    /**
     * his method handles the request came from client to get all students who scored more then
     * given percentage
     * @param standard standard of the students
     * @param percent required percentage
     * @return all students who scores more than requested percentage
     */
    @GetMapping(name="fetchStudentsByPercentage",value="/percentage/{standard}/{percent}")
    public ResponseEntity fetchStudentsByPercentage(@PathVariable int standard,@PathVariable float percent){
        List list=service.fetchStudentsByPercentage(standard,percent);
        return new ResponseEntity((new StudentsPercentageResponse(list)),HttpStatus.OK);
    }

    /**
     * To fetch the toppers of the requested subjects
     * @param standard toppers should be from this standard
     * @param subject this subject is used to fetch toppers
     * @return all students who are the toppers in the requested subject
     */
    @GetMapping(name="fetchToppersBySubject",value="/toppers/{standard}/{subject}")
    public ResponseEntity fetchToppersBySubject(@PathVariable int standard,@PathVariable String subject){
        List list=service.fetchToppersBySubject(standard,subject);
        return new ResponseEntity(list,HttpStatus.OK);
    }
    /**
     * To add a new subject to the school apart from default subjects
     * @param subjectRequest this subject should be added to the school
     * @return success response if subjects gets added to the school
     */
    @PostMapping(name="createSubjectEntry",value="/add-subject",consumes = "application/json")
    public ResponseEntity createSubjectEntry(@RequestBody SubjectRequest subjectRequest){
        if(service.addSubjectToStandard(subjectRequestModelDtoMapper.mapRequestToDto(subjectRequest))){
            return  new ResponseEntity(new SuccessResponse("Request Successful"," "),HttpStatus.OK);
        }
        return null;
    }

}
