package com.springboot.project.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 *To map subject from client to the java Class
 */
@Getter
@Setter
public class Subject {
    /**
     *Name of the subject
     */
    @JsonProperty("Name")
    private String subjectName;
    /**
     *Standard to which this subject belongs
     */
    @JsonProperty("Standard")
    private int subjectStandard;
}
