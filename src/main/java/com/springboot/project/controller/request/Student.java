package com.springboot.project.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 *To map a student(to java class) came from the client in the json format
 */
@Getter
@Setter
public class Student {

    /**
     *Roll no of the student
     */
    @JsonProperty("RollNo")
    private String studentRollNo;
    /**
     *Name of the student with roll no: <>studentRollNo</>
     */
    @JsonProperty("Name")
    private String studentName;
    /**
     *Standard of the student with name: <>studentName</>
     * and roll no: <>studentRollNo</>
     */
    @JsonProperty("Standard")
    private int studentStandard;
}
