package com.springboot.project.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 *To map the json request from the client to a proper java class
 */
@Getter
@Setter
public class Marks {
    /**
     *Roll no of the student
     */
    @JsonProperty("RollNo")
    private String rollNo;
    /**
     *Standard of the student
     */
    @JsonProperty("Standard")
    private int standard;
    /**
     *Subject
     */
    @JsonProperty("Subject")
    private String subject;
    /**
     *Marks scored in the  <>subject</> by student with roll no: <>rollNo</>
     * and standard: <>standard</>
     */
    @JsonProperty("Marks")
    private String  marks;
}
