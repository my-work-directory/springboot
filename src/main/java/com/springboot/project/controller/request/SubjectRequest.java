package com.springboot.project.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 *To map subject list came from client to the java Class
 */
@Getter
@Setter
public class SubjectRequest {
    /**
     *List of the subjects
     */
    @JsonProperty("Subject")
    private Subject subject;
}
