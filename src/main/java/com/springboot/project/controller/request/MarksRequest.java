package com.springboot.project.controller.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 *To map the json request (particular to the marks
 * of the students) from the client to a proper java class
 */
@Getter
@Setter
public class MarksRequest {
    /**
     *List of marks got from the client
     */
    @JsonProperty("MarksList")
    private ArrayList<Marks> marksList;
}
