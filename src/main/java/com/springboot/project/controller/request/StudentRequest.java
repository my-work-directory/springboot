package com.springboot.project.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
/**
 *To map the list of students came from the client to a Java class
 */
public class StudentRequest {
 /**
  *List of the students
  */
 @JsonProperty("Students")
 private List<Student> student;
}
